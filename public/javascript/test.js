//Global variables
var pageProject = false;
var pageAbout = false;
var pageContact = false;
var clickable = true;

//Transition to different pages
function transitionProject() {
  pageProject = true;
  document.getElementById("projects").className = document
    .getElementById("projects")
    .className.replace("d-none", "d-block");

  const element1 = document.querySelector(".animateStart");
  element1.classList.add("animated", "slideOutUp");

  element1.addEventListener("animationend", function() {
    document.getElementById("start").style.display = "none";
    slideUpProjects();
    element1.removeEventListener("animationend", this);
  });

  setTimeout(function() {
    element1.classList.add("d-none");
    element1.style.maxHeight = "0px";
    element1.style.minHeight = "0px";
    element1.style.height = "0px";
  }, 1000);
}

function transitionAbout() {
  pageAbout = true;
  document.getElementById("about").className = document
    .getElementById("about")
    .className.replace("d-none", "d-block");

  const element1 = document.querySelector(".animateStart");
  element1.classList.add("animated", "slideOutUp");

  element1.addEventListener("animationend", function() {
    document.getElementById("start").style.display = "none";
    slideUpAbout();
    element1.removeEventListener("animationend", this);
  });

  setTimeout(function() {
    element1.classList.add("d-none");
    element1.style.maxHeight = "0px";
    element1.style.minHeight = "0px";
    element1.style.height = "0px";
  }, 1000);
}

function transitionContact() {
  pageContact = true;
  document.getElementById("contact").className = document
    .getElementById("contact")
    .className.replace("d-none", "d-block");

  const element1 = document.querySelector(".animateStart");
  element1.classList.add("animated", "slideOutUp");

  element1.addEventListener("animationend", function() {
    document.getElementById("start").style.display = "none";
    slideUpContact();
    element1.removeEventListener("animationend", this);
  });

  setTimeout(function() {
    element1.classList.add("d-none");
    element1.style.maxHeight = "0px";
    element1.style.minHeight = "0px";
    element1.style.height = "0px";
  }, 1000);
}

//Slide down header

function slideDownHeader() {
  document.getElementById("slideHeader").style.visibility = "visible";
  const element3 = document.querySelector(".headerSlide");
  element3.classList.add("animated", "slideInDown");

  element3.addEventListener("animationend", function() {
    document.getElementById("bodyId").style.overflowY = "auto";
  });
}

/* function slideDownHeaderAbout() {
  document.getElementById("SlideIdAbout").style.visibility = "visible";
  const element3 = document.querySelector(".slideDownAbout");
  element3.classList.add("animated", "slideInDown");

  element3.addEventListener("animationend", function() {
    document.getElementById("bodyId").style.overflowY = "auto";
  });
} */

// Slide up pages

function slideUpProjects() {
  document.getElementById("projects").style.visibility = "visible";
  const element2 = document.querySelector(".animateProject");
  element2.classList.add("animated", "slideInUp");

  element2.addEventListener("animationend", function() {
    document.getElementById("start").style.display = "none";
    element2.classList.remove("slideInUp");
    slideDownHeader();
  });
}

function slideUpAbout() {
  document.getElementById("about").style.visibility = "visible";
  const element2 = document.querySelector(".animateAbout");
  element2.classList.add("animated", "slideInUp");

  element2.addEventListener("animationend", function() {
    document.getElementById("start").style.display = "none";
    element2.classList.remove("slideInUp");
    slideDownHeader();
  });
}

function slideUpContact() {
  document.getElementById("contact").style.visibility = "visible";
  const element2 = document.querySelector(".animateContact");
  element2.classList.add("animated", "slideInUp");

  element2.addEventListener("animationend", function() {
    document.getElementById("start").style.display = "none";
    element2.classList.remove("slideInUp");
    slideDownHeader();
  });
}

//Swap to projects
function projectPressed() {
  if (pageAbout == true) {
    swapAboutProject();
    console.log("swapAboutProject");
    console.log("pageAbout = " + pageAbout);
    console.log("pageContact = " + pageContact);
    console.log("pageProject = " + pageProject);
  } else if (pageContact == true) {
    swapContactProject();
    console.log("swapContactProject");
    console.log("pageAbout = " + pageAbout);
    console.log("pageContact = " + pageContact);
    console.log("pageProject = " + pageProject);
  }
}

//Swap to about
function aboutPressed() {
  if (pageProject == true) {
    swapProjectAbout();
    console.log("swapProjectAbout");
    console.log("pageAbout = " + pageAbout);
    console.log("pageContact = " + pageContact);
    console.log("pageProject = " + pageProject);
  } else if (pageContact == true) {
    swapContactAbout();
    console.log("swapContactAbout");
    console.log("pageAbout = " + pageAbout);
    console.log("pageContact = " + pageContact);
    console.log("pageProject = " + pageProject);
  }
}

//Swap to contact
function contactPressed() {
  if (pageProject == true) {
    swapProjectContact();
    console.log("swapProjectContact");
    console.log("pageAbout = " + pageAbout);
    console.log("pageContact = " + pageContact);
    console.log("pageProject = " + pageProject);
  } else if (pageAbout == true) {
    swapAboutContact();
    console.log("swapAboutContact");
    console.log("pageAbout = " + pageAbout);
    console.log("pageContact = " + pageContact);
    console.log("pageProject = " + pageProject);
  }
}

// Swap from project to about
function swapProjectAbout() {
  document
    .querySelector(".animateProject")
    .removeEventListener("animationend", this);
  document
    .querySelector(".animateStart")
    .removeEventListener("animationend", this);
  document
    .querySelector(".animateAbout")
    .removeEventListener("animationend", this);
  const swapPAelement1 = document.querySelector(".animateProject");
  swapPAelement1.classList.add("fadeOut");

  swapPAelement1.addEventListener("animationend", function swapPA1() {
    swapPAelement1.classList.remove("fadeOut");
    const swapPAelement2 = document.querySelector(".animateProject");
    swapPAelement2.classList.replace("d-block", "d-none");
    document.getElementById("projects").style.visibility = "hidden";

    const swapPAelement3 = document.querySelector(".animateAbout");
    document.getElementById("about").style.visibility = "visible";
    swapPAelement3.classList.replace("d-none", "d-block");
    swapPAelement3.classList.add("animated", "fadeIn");
    swapPAelement3.addEventListener("animationend", function swapPA2() {
      swapPAelement3.classList.remove("fadeIn");
      pageProject = false;
      pageAbout = true;
      pageContact = false;
      swapPAelement3.removeEventListener("animationend", swapPA2());
    });
    swapPAelement1.removeEventListener("animationend", swapPA1());
  });
}

//swap form project to contact
function swapProjectContact() {
  document
    .querySelector(".animateProject")
    .removeEventListener("animationend", this);
  document
    .querySelector(".animateStart")
    .removeEventListener("animationend", this);
  document
    .querySelector(".animateAbout")
    .removeEventListener("animationend", this);
  const swapPCelement1 = document.querySelector(".animateProject");
  swapPCelement1.classList.add("fadeOut");

  swapPCelement1.addEventListener("animationend", function swapPC1() {
    swapPCelement1.classList.remove("fadeOut");
    const swapPCelement2 = document.querySelector(".animateProject");
    swapPCelement2.classList.replace("d-block", "d-none");
    document.getElementById("projects").style.visibility = "hidden";

    const swapPCelement3 = document.querySelector(".animateContact");
    document.getElementById("contact").style.visibility = "visible";
    swapPCelement3.classList.replace("d-none", "d-block");
    swapPCelement3.classList.add("animated", "fadeIn");
    swapPCelement3.addEventListener("animationend", function swapPC2() {
      swapPCelement3.classList.remove("fadeIn");
      pageContact = true;
      pageProject = false;
      pageAbout = false;
      swapPCelement3.removeEventListener("animationend", swapPC2());
    });
    swapPCelement1.removeEventListener("animationend", swapPC1());
  });
}

//swap from about to project
function swapAboutProject() {
  document
    .querySelector(".animateProject")
    .removeEventListener("animationend", this);
  document
    .querySelector(".animateStart")
    .removeEventListener("animationend", this);
  document
    .querySelector(".animateAbout")
    .removeEventListener("animationend", this);

  const swapAPelement1 = document.querySelector(".animateAbout");
  swapAPelement1.classList.add("fadeOut");

  swapAPelement1.addEventListener("animationend", function swapAP1() {
    swapAPelement1.classList.remove("fadeOut");
    const swapAPelement2 = document.querySelector(".animateAbout");
    swapAPelement2.classList.replace("d-block", "d-none");
    document.getElementById("about").style.visibility = "hidden";

    const swapAPelement3 = document.querySelector(".animateProject");
    document.getElementById("projects").style.visibility = "visible";
    swapAPelement3.classList.replace("d-none", "d-block");
    swapAPelement3.classList.add("animated", "fadeIn");
    swapAPelement3.addEventListener("animationend", function swapAP2() {
      swapAPelement3.classList.remove("fadeIn");
      pageProject = true;
      pageAbout = false;
      pageContact = false;
      swapAPelement3.removeEventListener("animationend", swapAP2());
    });
    swapAPelement1.removeEventListener("animationend", swapAP2());
  });
}

//swap from about to contact
function swapAboutContact() {
  document
    .querySelector(".animateProject")
    .removeEventListener("animationend", this);
  document
    .querySelector(".animateStart")
    .removeEventListener("animationend", this);
  document
    .querySelector(".animateAbout")
    .removeEventListener("animationend", this);
  const swapACelement1 = document.querySelector(".animateAbout");
  swapACelement1.classList.add("fadeOut");

  swapACelement1.addEventListener("animationend", function swapAC1() {
    swapACelement1.classList.remove("fadeOut");
    const swapACelement2 = document.querySelector(".animateAbout");
    swapACelement2.classList.replace("d-block", "d-none");
    document.getElementById("about").style.visibility = "hidden";

    const swapACelement3 = document.querySelector(".animateContact");
    document.getElementById("contact").style.visibility = "visible";
    swapACelement3.classList.replace("d-none", "d-block");
    swapACelement3.classList.add("animated", "fadeIn");
    swapACelement3.addEventListener("animationend", function swapAC2() {
      swapACelement3.classList.remove("fadeIn");
      pageContact = true;
      pageAbout = false;
      pageProject = false;
      swapACelement3.removeEventListener("animationend", swapAC2());
    });
    swapACelement1.removeEventListener("animationend", swapAC1());
  });
}

//swap from contact to project
function swapContactProject() {
  document
    .querySelector(".animateProject")
    .removeEventListener("animationend", this);
  document
    .querySelector(".animateStart")
    .removeEventListener("animationend", this);
  document
    .querySelector(".animateAbout")
    .removeEventListener("animationend", this);
  const swapCPelement1 = document.querySelector(".animateContact");
  swapCPelement1.classList.add("fadeOut");

  swapCPelement1.addEventListener("animationend", function swapCP1() {
    swapCPelement1.classList.remove("fadeOut");
    const swapCPelement2 = document.querySelector(".animateContact");
    swapCPelement2.classList.replace("d-block", "d-none");
    document.getElementById("contact").style.visibility = "hidden";

    const swapCPelement3 = document.querySelector(".animateProject");
    document.getElementById("projects").style.visibility = "visible";
    swapCPelement3.classList.replace("d-none", "d-block");
    swapCPelement3.classList.add("animated", "fadeIn");
    swapCPelement3.addEventListener("animationend", function swapCP2() {
      swapCPelement3.classList.remove("fadeIn");
      pageProject = true;
      pageContact = false;
      pageAbout = false;
      swapCPelement3.removeEventListener("animationend", swapCP2());
    });
    swapCPelement1.removeEventListener("animationend", swapCP1());
  });
}

//swap from contact to about
function swapContactAbout() {
  document
    .querySelector(".animateProject")
    .removeEventListener("animationend", this);
  document
    .querySelector(".animateStart")
    .removeEventListener("animationend", this);
  document
    .querySelector(".animateAbout")
    .removeEventListener("animationend", this);
  const swapCAelement1 = document.querySelector(".animateContact");
  swapCAelement1.classList.add("fadeOut");

  swapCAelement1.addEventListener("animationend", function swapCA1() {
    swapCAelement1.classList.remove("fadeOut");
    const swapCAelement2 = document.querySelector(".animateContact");
    swapCAelement2.classList.replace("d-block", "d-none");
    document.getElementById("contact").style.visibility = "hidden";

    const swapCAelement3 = document.querySelector(".animateAbout");
    document.getElementById("about").style.visibility = "visible";
    swapCAelement3.classList.replace("d-none", "d-block");
    swapCAelement3.classList.add("animated", "fadeIn");
    swapCAelement3.addEventListener("animationend", swapCA2());
    swapCAelement1.removeEventListener("animationend", swapCA1());
  });
}

function swapCA2() {
  document.querySelector(".animateAbout").classList.remove("fadeIn");
  pageProject = false;
  pageAbout = true;
  pageContact = false;
  document
    .querySelector(".animateAbout")
    .removeEventListener("animationend", swapCA2());
}
