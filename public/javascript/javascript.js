//Global variables
window.onhashchange = TESTswap;

const classProject = document.querySelector(".animateProject");
const classAbout = document.querySelector(".animateAbout");
const classContact = document.querySelector(".animateContact");

//Slide down header

function slideDownHeader() {
  document.getElementById("slideHeader").style.visibility = "visible";
  const header = document.querySelector(".headerSlide");
  header.classList.add("animated", "slideInDown");

  header.addEventListener(
    "animationend",
    () => {
      document.getElementById("bodyId").style.overflowY = "auto";
      header.removeEventListener("animationend", this, false);
    },
    {
      once: true
    }
  );
}

function slideUp() {
  const start = document.querySelector(".animateStart");
  start.classList.add("animated", "slideOutUp");
  start.addEventListener(
    "animationend",
    () => {
      start.classList.add("d-none");
      document
        .querySelector(".animateAbout")
        .classList.replace("d-none", "d-block");
      start.style.maxHeight = "0px";
      start.style.minHeight = "0px";
      start.style.height = "0px";
      loadPage();
      start.removeEventListener("animationend", this, false);
    },
    {
      once: true
    }
  );
}

function loadPage() {
  var test = location.hash.toString();
  var testString = location.hash.toString().substring(6, location.hash.length);
  document.getElementById(testString).style.visibility = "visible";

  const page = document.querySelector(
    ".animate" +
      test
        .substring(6, location.hash.length)
        .charAt(0)
        .toUpperCase() +
      location.hash
        .toString()
        .substring(5, location.hash.length)
        .slice(2)
  );
  page.classList.add("animated", "slideInUp");
  page.classList.replace("d-none", "d-block");
  page.addEventListener(
    "animationend",
    () => {
      document.getElementById("start").style.display = "none";
      slideDownHeader();
      page.classList.remove("animated");
      page.classList.remove("slideInUp");
      page.removeEventListener("animationend", this);
    },
    {
      once: true
    }
  );
}

function swap() {
  if (location.hash.toString() != "#" || location.hash.toString() != "") {
    hideAll();
  }

  if (
    location.hash.toString() == "#startproject" ||
    location.hash.toString() == "#startabout" ||
    location.hash.toString() == "#startcontact"
  ) {
    slideUp();
  }
  if (location.hash.toString() == "#project") {
    classProject.classList.replace("d-none", "d-block");
    document.getElementById("project").style.visibility = "visible";
  }

  if (location.hash.toString() == "#about") {
    classAbout.classList.replace("d-none", "d-block");
    document.getElementById("about").style.visibility = "visible";
  }

  if (location.hash.toString() == "#contact") {
    classContact.classList.replace("d-none", "d-block");
    document.getElementById("contact").style.visibility = "visible";
  }
}

function hideAll() {
  classProject.classList.replace("d-block", "d-none");
  document.getElementById("project").style.visibility = "hidden";

  classAbout.classList.replace("d-block", "d-none");
  document.getElementById("about").style.visibility = "hidden";

  classContact.classList.replace("d-block", "d-none");
  document.getElementById("contact").style.visibility = "hidden";
}

function TESTswap() {
  if (location.hash.toString() != "#" && location.hash.toString() != "") {
    hideAll();
  }

  if (
    location.hash.toString() == "#startproject" ||
    location.hash.toString() == "#startabout" ||
    location.hash.toString() == "#startcontact"
  ) {
    slideUp();
  }
  if (location.hash.toString() == "#project") {
    classProject.classList.replace("d-none", "d-block");
    document.getElementById("project").style.visibility = "visible";

    classProject.classList.add("animated");
    classProject.classList.add("fadeIn");

    classProject.addEventListener(
      "animationend",
      () => {
        classProject.classList.remove("fadeIn");
        classProject.classList.remove("animated");
        classProject.removeEventListener("animationend", this);
      },
      {
        once: true
      }
    );
  }

  if (location.hash.toString() == "#about") {
    classAbout.classList.replace("d-none", "d-block");
    document.getElementById("about").style.visibility = "visible";

    classAbout.classList.add("fadeIn");
    classAbout.classList.add("animated");

    classAbout.addEventListener(
      "animationend",
      () => {
        classAbout.classList.remove("fadeIn");
        classAbout.classList.remove("animated");
        classAbout.removeEventListener("animationend", this);
      },
      {
        once: true
      }
    );
  }

  if (location.hash.toString() == "#contact") {
    classContact.classList.replace("d-none", "d-block");
    document.getElementById("contact").style.visibility = "visible";

    classContact.classList.add("animated");
    classContact.classList.add("fadeIn");

    classContact.addEventListener(
      "animationend",
      () => {
        classContact.classList.remove("fadeIn");
        classContact.classList.remove("animated");
        classContact.removeEventListener("animationend", this);
      },
      {
        once: true
      }
    );
  }
}
function TESThideAll() {
  classProject.classList.add("animated");
  classProject.classList.add("fadeOut");
  classProject.addEventListener(
    "animationend",
    () => {
      classProject.classList.replace("d-block", "d-none");
      classProject.classList.remove("fadeOut");
      classProject.classList.remove("animated");
      classProject.removeEventListener("animationend", this);
    },
    { once: true }
  );

  classAbout.classList.add("animated");
  classAbout.classList.add("fadeOut");
  classAbout.addEventListener(
    "animationend",
    () => {
      classAbout.classList.replace("d-block", "d-none");
      classAbout.classList.remove("fadeOut");
      classAbout.classList.remove("animated");
      classAbout.removeEventListener("animationend", this);
    },
    { once: true }
  );

  classContact.classList.add("animated");
  classContact.classList.add("fadeOut");
  classContact.addEventListener(
    "animationend",
    () => {
      classContact.classList.replace("d-block", "d-none");
      classContact.classList.remove("fadeOut");
      classContact.classList.remove("animated");
      classContact.removeEventListener("animationend", this);
    },
    { once: true }
  );
}
